libaudio-scan-perl (1.01-2) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

  [ gregor herrmann ]
  * debian/control: update Build-Depends for cross builds.
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.

  [ gregor herrmann ]
  * Declare compliance with Debian Policy 4.6.1.
  * Set Rules-Requires-Root: no.
  * Annotate test-only build dependencies with <!nocheck>.

 -- gregor herrmann <gregoa@debian.org>  Sat, 30 Jul 2022 03:25:19 +0200

libaudio-scan-perl (1.01-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * debian/watch: drop opts (uversionmangle, repacksuffix).
    Repackaging is gone since a couple of releases.
  * Drop disable-failing-test-sparc.patch.
    sparc is gone since a couple of years.
  * Declare compliance with Debian Policy 4.1.5.
  * Bump debhelper compatibility level to 10.

 -- gregor herrmann <gregoa@debian.org>  Tue, 31 Jul 2018 18:08:58 +0200

libaudio-scan-perl (1.00-1) unstable; urgency=medium

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Florian Schlichting ]
  * Import upstream version 1.00
  * Declare compliance with Debian Policy 4.1.4

 -- Florian Schlichting <fsfs@debian.org>  Mon, 14 May 2018 14:47:01 +0200

libaudio-scan-perl (0.99-1) unstable; urgency=medium

  [ Alex Muntada ]
  * Remove inactive pkg-perl members from Uploaders.

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.2 (no changes needed)

  [ Florian Schlichting ]
  * Import upstream version 0.99
  * Refresh patch
  * Add myself to Uploaders

 -- Florian Schlichting <fsfs@debian.org>  Wed, 20 Dec 2017 00:35:23 +0100

libaudio-scan-perl (0.98-1) unstable; urgency=medium

  * Team upload

  * Import upstream version 0.98
  * Add upstream metadata
  * Export all hardening buildflags
  * Declare compliance with Debian Policy 4.1.0

 -- Florian Schlichting <fsfs@debian.org>  Mon, 18 Sep 2017 23:44:39 +0200

libaudio-scan-perl (0.96-1) unstable; urgency=medium

  * Team upload

  [ Axel Beckert ]
  * Remove Maximilian Gaß from Uploaders (no more active according to himself)

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Switch repackaging framework to Files-Excluded method.
  * Remove Jonathan Yu from Uploaders. Thanks for your work!

  [ Florian Schlichting ]
  * Import upstream version 0.96 (closes: #802731)
  * Drop Files-Excluded, zlib is no longer bundled in the upstream tarball
  * Update upstream contact address and copyright years
  * Declare compliance with Debian Policy 3.9.8
  * Fix file name in d/copyright

 -- Florian Schlichting <fsfs@debian.org>  Tue, 22 Nov 2016 21:35:24 +0100

libaudio-scan-perl (0.93+dfsg-3) unstable; urgency=low

  * Team upload

  [ Alessandro Ghedini ]
  * Email change: Alessandro Ghedini -> ghedo@debian.org

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * Update debian/repack.stub.

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Florian Schlichting ]
  * Switch dh compatibility to level 9 to enable passing of hardening flags
  * Declare compliance with Debian Policy 3.9.5
  * Fix comma-separated-files-in-dep5-copyright warning

 -- Florian Schlichting <fsfs@debian.org>  Sat, 08 Mar 2014 22:14:39 +0100

libaudio-scan-perl (0.93+dfsg-2) unstable; urgency=low

  * Team upload.
  * Remove unneeded hunk about missing newline at the end of file from the
    patch. dpkg-source from squeeze chokes over it.

 -- gregor herrmann <gregoa@debian.org>  Sat, 17 Sep 2011 15:06:07 +0200

libaudio-scan-perl (0.93+dfsg-1) unstable; urgency=low

  * New upstream release
  * Add disable-failing-test-sparc.patch to disable a failing test on sparc

 -- Alessandro Ghedini <al3xbio@gmail.com>  Sat, 17 Sep 2011 12:47:26 +0200

libaudio-scan-perl (0.92+dfsg-1) unstable; urgency=low

  * New upstream release
  * Add copyright info for src/jenkins_hash.c

 -- Alessandro Ghedini <al3xbio@gmail.com>  Mon, 12 Sep 2011 11:10:14 +0200

libaudio-scan-perl (0.91+dfsg-1) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

  [ Alessandro Ghedini ]
  * New upstream release

 -- Alessandro Ghedini <al3xbio@gmail.com>  Wed, 07 Sep 2011 10:58:55 +0200

libaudio-scan-perl (0.90+dfsg-1) unstable; urgency=low

  * New upstream release

 -- Alessandro Ghedini <al3xbio@gmail.com>  Thu, 04 Aug 2011 18:59:47 +0200

libaudio-scan-perl (0.88+dfsg-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Alessandro Ghedini ]
  * New upstream release
  * Bump Standards-Version to 3.9.2
  * Re-order (Build-)Depends (just cosmetics)
  * Fix BSD-3-clause license paragraphs
  * Fix Expat and Zlib licenses formatting

 -- Alessandro Ghedini <al3xbio@gmail.com>  Tue, 02 Aug 2011 16:10:27 +0200

libaudio-scan-perl (0.87+dfsg-1) unstable; urgency=low

  * New upstream release
  * Rewrite control description
  * No longer run POD tests
  * Refresh copyright information

 -- Jonathan Yu <jawnsy@cpan.org>  Mon, 21 Mar 2011 23:10:41 -0400

libaudio-scan-perl (0.86+dfsg-1) unstable; urgency=low

  * New upstream release
  * Build-Depends-Indep on libtest-warn-perl, libtest-pod-perl and
    libtest-pod-coverage-perl
  * Add myself to Uploaders
  * Enable tests on POD

 -- Alessandro Ghedini <al3xbio@gmail.com>  Sat, 19 Mar 2011 13:40:46 +0100

libaudio-scan-perl (0.85+dfsg-1) unstable; urgency=low

  * Initial release (closes: #524789)

 -- Maximilian Gass <mxey@cloudconnected.org>  Wed, 02 Mar 2011 21:09:13 +0100
